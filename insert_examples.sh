#!/bin/bash

# Parse command line arguments
for i in "$@"
do
case $i in
    --host=*)
    HOST="${i#*=}"
    ;;
	--environment=*)
    ENVIRONMENT="${i#*=}"
	;;
	--reindex-script=*)
	REINDEX_SCRIPT="${i#*=}"
	;;
    *)
    # unknown option
	echo "Error: Unknown option ${i%=*}"
	exit 1
    ;;
esac
done

if [[ -z "${HOST}" ]]; then
	echo "Error: --host must be specified"
	exit 1
fi

if [[ -z "${ENVIRONMENT}" ]]; then
	echo "Error: --environment must be specified"
	exit 1
fi

ALIAS="${ENVIRONMENT}-sndcatalogue"

if [[ -z "${HOST}" ]]; then
	echo "Error: --host must be specified"
	exit 1
fi

if [[ -z "${ENVIRONMENT}" ]]; then
	echo "Error: --environment must be specified"
	exit 1
fi

# Index example catalogue documents
for filepath in example-documents/sndcatalogue/*.json; do
    SLUG=$(basename $filepath .json)
    echo "Indexing: $SLUG"

    curl -fsS -X PUT "${HOST}"/"${ALIAS}/_doc/${SLUG}" -H 'Content-Type: application/json' -d @${filepath} > /dev/null
done

echo ""
echo "Documents overview:"
curl -fsS -X POST "${HOST}/_sql?format=txt" -H 'Content-Type: application/json' -d '
	{
		"query": "SELECT datasetIdentifier, versionNumber, title.sv FROM \"'${ALIAS}'\" WHERE isLatestVersion = true"
	}'
