## Ny struktur på CV

{
    "value",
    "description",
    "identifier",
    "uri",
    "parent",
    "selected",
    "vocabulary"
}


## Status
går det ta bort fältet status, vi behöver nog bara withdrawnDate


## Fältnamn bytt
<pre>
principal.slug => principal.identifier
slug => datasetIdentifier
doi => conceptDoi
modified => modifiedDate
abstract => description
relatedStudy => relatedDataset
collection.relatedStudy => collection.relatedDataset
*.diaryNumber => *.registrationNumber
funding.organisation => får subproperty "name" rorId flyttas från funding till funding.organisation
ethicalReview.organisation => ethicalReview.organisation.name
language.value => language.name
availabilityStatus => accessibilityLevel byta namn behhöver göras på flera ställen.
personalInformation => personalData
containsPersonalInformation => containsPersonalData
caseCount => caseQuantity
variableCount => variableQuantity
</pre>

## Lägg till fält
<pre>
otherPrincipal.organisationNumber
language.iso639_1
language.iso639_3
geographicLocation.iso3166_1
bioBank lägg till alla subfält som icke idexerade fält.
subject.identifier - används för att länk parent
speciesAndTaxon.identifier
geographicLocation.identifier
kindOfDataFormat.identifier
typeOfRemains.identifier
</pre>

## Slå ihop keyword & freeKeyword
Använd samma struktur som cv?

## ta bort
<pre>
id - ta bort från alla ställen? använd identifier för maskinläsbara id
metadataProfile - behövs detta för katalogen?
citationRequirement - ta bort? finns inte i DORIS men visas i katalogen
restrictions - ta bort. visas inte i katalogen
accessConditions - visas inte i katalogen, ska det tas bort?
collection.relatedStudy.slug - använd identifier istället
geographicLocation.slug - ta bort
license.id - ta bort
subject.id
keyword.id
typeOfInvestigation.id
timePeriod.startEra.id
timePeriod.endEra.id
timeMethod.id
studyDesign.id
unitOfAnalysis.id
samplingProcedure.id
speciesAndTaxon.id
geographicLocation.id
lowestGeographicUnit.id
highestGeographicUnit.id
kindOfDataFormat.id
modeOfCollection.id
instrument.type.id
spatialResolutionUnit.id
dataSource.id
temporalResolutionUnit.id
typeOfRemains.id
externalResource.role.id

containsPersonalData & containsBiobank Ta bort från mappningen och sätt null på containsPersonalData och bioBank?
citation - se över om vi kan ta bort fältet. Används för extern citering just nu.
</pre>