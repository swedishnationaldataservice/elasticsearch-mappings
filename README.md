# elasticsearch-mappings
Elastic Search mappings for SND:s metadata

## Test and develop localy

To run elasticsearch: `docker-compose up -d`

### To create a new index for `feat/dataset-restructure`:

1. Create index: `./deploy.sh --host=http://localhost:9200 --environment=dev-dataset-restructure`
2. Insert examples: `./insert_examples.sh --host=http://localhost:9200 --environment=dev-dataset-restructure`

## Search the documents

### Simple free text search

```bash
curl -XPOST localhost:9200/sndcatalogue-en/study/_search -d'
{
  "query": {
    "simple_query_string" : {"query": "swedish"}
  }
}
'

```

### Filter on keyword value combined with free text search

```bash
curl -XPOST localhost:9200/sndcatalogue-en/study/_search -d'
{
  "query": {
    "bool": {
      "must": {
        {"simple_query_string": {"query": "swedish"}}
      },
      "filter": [
        {
          "term": {
            "keyword.value.raw": "mass media"
          }
        }
      ]
    }
  }
}'
```