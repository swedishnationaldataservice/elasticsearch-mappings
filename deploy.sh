#!/bin/bash

# Usage: deploy.sh --host=address_to_elasticsearch --environment=prod [--reindex-script=painless-script]
# If no existing index is found, a new index and alias is created, otherwise the old index is reindexed
# to the new one and the alias repointed.
#
# --reindex-script: 
#   A painless script for modifying documents before reindexing in the new index. This can e.g. be
#   used for fixing problems with type mismatches between the old and the new mapping.
#
# Example: deploy.sh --host=https://some-host.com --environment=prod
# Deploys to https://some-host.com with index/alias based on "prod".

# Parse command line arguments
for i in "$@"
do
case $i in
    --host=*)
    HOST="${i#*=}"
    ;;
	--environment=*)
    ENVIRONMENT="${i#*=}"
	;;
	--reindex-script=*)
	REINDEX_SCRIPT="${i#*=}"
	;;
    *)
    # unknown option
	echo "Error: Unknown option ${i%=*}"
	exit 1
    ;;
esac
done

if [[ -z "${HOST}" ]]; then
	echo "Error: --host must be specified"
	exit 1
fi

if [[ -z "${ENVIRONMENT}" ]]; then
	echo "Error: --environment must be specified"
	exit 1
fi

NOW=$(date -u +'%Y-%m-%d-%H_%M_%S')
ALIAS="${ENVIRONMENT}-sndcatalogue"
NEW_INDEX="${ALIAS}_${NOW}"
OLD_INDEX=$(curl -fs "${HOST}"/_alias/"${ALIAS}" | cut -d '"' -f 2)

if [[ -z "${OLD_INDEX}" ]]; then
	REINDEX=0
else
	REINDEX=1
fi

if [[ ! -z "${REINDEX_SCRIPT}" ]]; then
	REINDEX_SCRIPT=",\"script\":{\"source\": \"${REINDEX_SCRIPT}\",\"lang\":\"painless\"}"
fi

cleanUpAndExit() {
	echo "Error on line $1!"
	echo "Cleaning up..."

	if [[ $REINDEX -eq 1 ]]; then
		# Set old index as writeable
		echo "Setting old index as writeable..."
		curl -fsS -X PUT "${HOST}"/"${OLD_INDEX}"/_settings -H 'Content-Type: application/json' -d '{"index.blocks.write": false}' > /dev/null
	fi

	# Delete new index
	echo "Deleting new index..."
	curl -fsS -X DELETE "${HOST}"/"${NEW_INDEX}" > /dev/null

	echo "Clean up done."
	exit 1
}

# On any error, try to clean up and then exit
trap 'cleanUpAndExit $LINENO' ERR

# Create new index
echo "Creating new index..."
curl -fsS -X PUT "${HOST}"/"${NEW_INDEX}" -H 'Content-Type: application/json' -d @'sndcatalogue.json' > /dev/null

if [[ $REINDEX -eq 1 ]]; then

	# Set old index as read only to prevent updates while reindexing
	echo "Setting old index as read only..."
	curl -fsS -X PUT "${HOST}"/"${OLD_INDEX}"/_block/write -d '' > /dev/null

	# Reindex from old index to new index
	echo "Reindexing..."
	curl -fsS -X POST "${HOST}"/_reindex -H 'Content-Type: application/json' -d "{\"source\":{\"index\":\"${OLD_INDEX}\"},\"dest\":{\"index\":\"${NEW_INDEX}\"}${REINDEX_SCRIPT}}" > /dev/null
fi

# Beyond this point cleanup is not applicable, so turn off error trap
trap - ERR
# Abort in case of error
set -e

# Point alias to new index
echo "Updating alias..."
if [[ $REINDEX -eq 1 ]]; then
	curl -fsS -X POST "${HOST}"/_aliases -H 'Content-Type: application/json' -d "{\"actions\":[{\"remove\":{\"index\":\"${OLD_INDEX}\",\"alias\":\"${ALIAS}\"}},{\"add\":{\"index\":\"${NEW_INDEX}\",\"alias\":\"${ALIAS}\"}}]}" > /dev/null
else
	curl -fsS -X POST "${HOST}"/_aliases -H 'Content-Type: application/json' -d "{\"actions\":[{\"add\":{\"index\":\"${NEW_INDEX}\",\"alias\":\"${ALIAS}\"}}]}" > /dev/null
fi

if [[ $REINDEX -eq 1 ]]; then
	# Delete old index
	echo "Deleting old index..."
	curl -fsS -X DELETE "${HOST}"/"${OLD_INDEX}" > /dev/null
fi

echo "Done."
